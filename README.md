CERN Phonebook
==============

This generates a more useful "phonebook" if you run it on lxplus.

Once you run it you can put the resulting `table.html` file on your
website. You can [create a website through CERN][1].

There's an `.htaccess` file in here that should help you set
permissions. Probably better not to make everyone's email and office
numbers public.

Also check out the [more useful cern website documentation][2]

[1]: https://webservices.web.cern.ch/webservices/Services/CreateNewSite/Default.aspx
[2]: https://webeos.docs.cern.ch/create_site/


I can add more information, from the utility docs:
```
Valid TTT values are: surname, firstname, phone, otherphone, mobile,
fax, office, department, group, pobox, email, displayname,
organization, ccid, login, homedir, last, uid, gid, uac, type,
emailnick, company, shell ,secid.
```
