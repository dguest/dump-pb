#!/usr/bin/env bash

for f in {a..z}
do
    for s in {a..z}
    do
        OUTFILE=${f}${s}.txt
        if [[ ! -f $OUTFILE ]]; then
            phonebook ${f}${s} -t firstname -t surname -t login -t group > $OUTFILE
            wc $OUTFILE
        else
            echo "already have $OUTFILE"
        fi
    done
done

ALL=all.txt
if [[ ! -f ${ALL} ]]; then
    cat [a-z][a-z].txt | sort -u > ${ALL}
else
    echo "alreadh have ${ALL}"
fi

TAB=table.html

cat<<EOF > ${TAB}
<head>
<style type="text/css">
th {
  text-align: left;
}
</style>
</head>
EOF

cat<<EOF >> ${TAB}
<table>
<tr><th>First Name</th><th>Last Name</th><th>User Name</th><th>Group</th></tr>
EOF
cat ${ALL} | awk -F ';'\
                 '{print "<tr><td>"$1"</td><td>"$2"</td><td>"$3"</td><td>"$4"</td></tr>"}' \
                 >> ${TAB}
echo "</table>" >> ${TAB}

cat<<EOF >> ${TAB}
Like what you see? Hate it? <a href="https://gitlab.cern.ch/dguest/dump-pb">Click here to make a feature request!</a>
EOF
